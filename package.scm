(use-modules
  (guix build-system python)
  ((guix licenses) #:prefix license:)
  (guix packages)
  (guix download)
  (guix git-download)
  (guix build utils)
  (gnu packages python)
  (gnu packages python-web)
  (gnu packages python-xyz)
  (gnu packages python-build)
  (gnu packages python-crypto)
  (gnu packages time)
  (gnu packages base)
  (gnu packages check)
  (gnu packages django)
  (gnu packages xml)
  (gnu packages databases))

(define-public python-django-3.0
  (package
    (name "python-django")
    (version "3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "Django" version))
              (sha256
               (base32
                "1bh4i2jvagg1drvmcs8n1fsimg96d3zi9h273z2pn57dbrp9p36r"))))
    (build-system python-build-system)
    (arguments
     '(#:tests? #f))
    ;; TODO: Install extras/django_bash_completion.
    (native-inputs
     `(("tzdata" ,tzdata)
       ;; Remaining packages are test requirements taken from
       ;; tests/requirements/py3.txt
       ("python-docutils" ,python-docutils)
       ;; optional for tests: ("python-geoip2" ,python-geoip2)
       ("python-jinja2" ,python-jinja2) ; >= 2.7
       ;; optional for tests: ("python-memcached" ,python-memcached)
       ("python-numpy" ,python-numpy)
       ("python-pillow" ,python-pillow)
       ("python-pyyaml" ,python-pyyaml)
       ;; optional for tests: ("python-selenium" ,python-selenium)
       ("python-sqlparse" ,python-sqlparse)
       ("python-tblib" ,python-tblib)
       ("python-selenium" ,python-selenium)))
    (propagated-inputs
     `(("python-argon2-cffi" ,python-argon2-cffi)
       ("python-asgiref" ,python-asgiref)
       ("python-bcrypt" ,python-bcrypt)
       ("python-pytz" ,python-pytz)))
    (home-page "https://www.djangoproject.com/")
    (synopsis "High-level Python Web framework")
    (description
     "Django is a high-level Python Web framework that encourages rapid
development and clean, pragmatic design.  It provides many tools for building
any Web site.  Django focuses on automating as much as possible and adhering
to the @dfn{don't repeat yourself} (DRY) principle.")
    (license license:bsd-3)
    (properties `((python2-variant . ,(delay python2-django))
                  (cpe-name . "django")))))

(let ((commit "86db1394416ba34f5aebd32e98013a553aa650df")
      (revision "0"))
  (package
   (name "python-django-request")
   (version (git-version "1.6.1" revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://gitlab.com/manifeste-ecologique/django-request")
           (commit commit)))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0vx0g0hlb3h2vhd6m5a2pc8j8a0qziccab7a0d5mp0gvf78pimhp"))))
   (build-system python-build-system)
   (arguments
    `(#:phases
      (modify-phases %standard-phases
        (replace 'check
          (lambda _
            (invoke "python" "runtests.py")
            #t)))))
   (propagated-inputs
    (list python-dateutil python-django-3.0 python-six))
   (native-inputs
    (list python-sqlparse python-mock))
   (home-page "https://django-request.readthedocs.io")
   (synopsis
    "statistics module for django")
   (description
    "django-request is a statistics module for django.  It stores requests in a database for admins to see, it can also be used to get statistics on who is online etc.")
   (license license:expat)))
